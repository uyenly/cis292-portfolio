#Uyen Ly
#Update
#Final project
def updateEntry(book):#update existing entries in Address Book
    #user input 
    fname = input("Enter first name: ").upper().strip()
    lname = input("Enter last name: ").upper().strip()
    midin = input("Enter middle initial: ").upper().strip()

    print()

    namekey = ''.join([fname, lname, midin])#create key reference to object in dict
    
    if namekey not in book: #check if name exists in dictionary
        print("Name not in address book")
        print()
        print()
    else:#user option
        print("Enter number to choose what to update.")
        print("1-Address")
        print("2-Phone number")
        print("3-Email address")
        print("4-QUIT")
        print()
        print()

        choice = int(input("Choice: "))
        
        while choice != 4:
            
            if choice == 1:
                address = input("Enter address: ")
                book[namekey].setAddress(address)

            elif choice == 2:
                try:
                    phone_num = input("Enter phone number in xxx-xxx-xxxx format: ")
                    phone_parts = phone_num.split('-')#stored in list
                    
                    if phone_parts[0].isdigit() and len(phone_parts[0]) == 3:
                        area_code = phone_parts[0]
                    else:
                        phone_parts[0]='111'
                        area_code = phone_parts[0]
                    book[namekey].setAreaCode(area_code)
    
                    if phone_parts[1].isdigit() and len(phone_parts[1]) == 3:
                        extension = phone_parts[1]
                    else:
                        phone_parts[1]='111'
                        extension = phone_parts[1]
                    book[namekey].setPhoneExt(extension)
    
                    if phone_parts[2].isdigit() and len(phone_parts[2]) == 4:
                        digits= phone_parts[2]
                    else:
                        phone_parts[2]='111'
                        digits= phone_parts[2]
                    book[namekey].setPhoneDigits(digits)
                    
                except:
                    print("Invalid input")
                    print()

            elif choice == 3:
                email_add = input("Enter email address: ")
                email_parts = email_add.split('@')#stored in list
                
                username = email_parts[0]
                domain = email_parts[1]
                
                book[namekey].setEmailName(username)
                book[namekey].setEmailDomain(domain)
                
            print()
            choice=4




                




