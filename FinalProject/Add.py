#written by Tim O'Connor
#to be imported into final group project

import Entry
import PrintOut

class notRight(Exception):
    def __init__(self, message):
        self.message = message
    

def addEntry(book): #book is a dictionary
    #book contains 'name':'AddressEntry' pairs
    correct = 'n'
    while correct != 'y':
        try:
            print('Enter the following for the new contact in AddressBook...')
            print() #spacer
            first = input('First Name: ')
            print() #spacer
            last = input('Last Name: ')
            print() #spacer
            middle = input('Middle Initial: ')
            print() #spacer
            print('--------') #visual break
            print()

            #clean up what came in
            first = first.strip().upper()
            last = last.strip().upper()
            middle = middle.strip().upper()
            print(first, last, middle)
            print() #spacer

            #check against user for correctness
            good = input('Is this correct? y/n: ')
            if good != 'y':
                raise notRight('User wishes to re-enter name')
            print()

            #check for name in address book
            nameKey = first+last+middle
            if nameKey in book:
                raise notRight('That name is already in the Address Book try \
                                with a different name or use a nickname')

            #create the class instance
            newEntry = Entry.AddressEntry(first,last,middle)

            correct = 'y'  #to allow the loop break
        except notRight as e:
            print(e)
        except ValueError as v:
            print(v)

    #add details
    detailsDone = 'n'
    while detailsDone != 'y':
        try:
            address = input('Address: ')
            newEntry.setAddress(address)

            areaCode = int(input('Area Code: '))
            newEntry.setAreaCode(areaCode)

            phoneExt = int(input('Phone Extension: '))
            newEntry.setPhoneExt(phoneExt)

            phoneDigits = int(input('Phone Digits: '))
            newEntry.setPhoneDigits(phoneDigits)

            email = input('Email Address: ')
            newEntry.setEmailName(email)

            emailDomain = input('Email Domain: ')
            newEntry.setEmailDomain(emailDomain)
            print() #spacer
            print('--------') #visual break
            print(newEntry)
            print() #spacer
            

            detailsDone = input('Is this correct? y/n: ') #break loop

        except ValueError as v:
            print(v)
        except:
            print('error')

    book[nameKey] = newEntry
