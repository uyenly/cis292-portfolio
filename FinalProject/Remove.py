import Entry
def removeEntry(book):
    first = str(input("What is your first name?: ")).upper()
    first = first.replace(" ", "") #cleaned
    #tests that MI is not more than 1 letter
    while True:
        initial = str(input("What is your middle initial?: ")).upper()
        if len(initial) > 1:
            print("Not valid. Try Again.")
        else:
            break
    last = str(input("What is your last name?: ")).upper()
    last = last.replace(" ", "") #clean last
    
    FullName = first + last + initial
        
    if FullName in book:
        print("Here is your entry:", FullName)
        if input("Would you like to delete the entry? [y/n]: ").lower() == 'y':
            del book[FullName]
            print("Deleted.")
        else:
            print("Cancelled by user, returning...\n")
    else:
        print("Name not found.")
        print("Here is a list of last names in Address Book:")
        for key in book:
            if book[key].getLName() == last:
                print(book[key])
