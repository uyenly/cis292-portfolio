# Final Project compiled by:
# Tim O'C , Michael Melendez, Uyen Ly, Joe

import Entry
import Add
import Search
import Remove
import Update
import PrintOut


def menu():
    print("Choose one option: ")
    print("1 Add a new entry to address book ")
    print("2 Update entry in the address book")
    print("3 Remove an entry from the address book")
    print("4 Search for an entry in the book")
    print("5 Print out one section of the address book")
    print("6 Print out the entire address book")
    print("7 EXIT")
    choice = int(input("Enter your choice"))
    return choice
                 
addressBook={}   #addressbook is initially entry

userChoice= menu()
while userChoice != 7:

    if userChoice == 1:  #add entry to addressBook
        Add.addEntry(addressBook)
        print()

    elif userChoice == 2:  #update entry to addressBook
        Update.updateEntry(addressBook)
        print()

    elif userChoice == 3:  #remove entry to addressBook
        Remove.removeEntry(addressBook)
        print()

    elif userChoice == 4:  #search for entry in addressBook
        Search.searchEntry(addressBook)
        print()

    elif userChoice== 5:  #print out one section of addressBook (A's, B's, etc)
         PrintOut.printSection(addressBook)
         print()

    elif userChoice == 6:  #print out entire book
         PrintOut.printAll(addressBook)
         print()

    userChoice = menu()
    
    
    


