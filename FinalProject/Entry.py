#this class represents one entry in an AddressBook

class AddressEntry:
    def __init__(self,firstName,lastname,mid):
        self.__fname= firstName
        self.__lname= lastname
        self.__minit = mid
        self.__address= "Unknown"
        self.__areaCode = 000
        self.__phoneExt= 555
        self.__phonedigits = 1111
        self.__emailName = "unknown"
        self.__emailDomain = "domain.com"


    def getFName(self):
        return self.__fname
    
    def getLName(self):
        return self.__lname

    def setFName(self, newfname):
        self.__fname = newfname

    def setLName(self, newlname):
        self.__lname = newlname

    def setMid(self, mid):
        self.__minit = mid
    
    def setAddress(self, newAddress):
        self.__address = newAddress

    def setAreaCode(self, newArea):
        self.__areaCode = newArea

    def setPhoneExt(self, newExt):
        self.__phoneExt = newExt

    def setPhoneDigits(self, newDigits):
        self.__phonedigits = newDigits

    def setEmailName(self,neweName):
        self.__emailName = neweName

    def setEmailDomain(self,neweDomain):
        self.__emailDomain = neweDomain

    def __str__(self):
        return self.__fname + " " + self.__minit +" "+self.__lname + "\n" \
               + self.__address + "\n" +"Phone:" + str(self.__areaCode) + "-" \
               + str(self.__phoneExt) + "-" + str(self.__phonedigits)\
               +'\n' + self.__emailName + '@' + self.__emailDomain
