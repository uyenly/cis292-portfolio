##Programmed by Michael Melendez ##
##4-25-18                        ##
##SEARCH Module                  ##

def searchEntry(book):
    firstname = input('What is the first name?: ')
    middle = input('What is the middle initial?: ')
    lastname = input('What is the last name?: ')
    firstname = firstname.upper()
    middle = middle.upper()
    lastname = lastname.upper()
    fullname = firstname + lastname + middle 
    if fullname in book:
        print(fullname, 'is already in the book.')
        print(book[fullname])
    else:
        print(fullname, 'is not in the dictionary.')
        print('Same last names shown: ')
        for key in book:
            if key.getLName().upper() == lastname:
                print(key)
