package com.example.android.justjava;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

    //This app is a Welcome screen before entering the main application
public class SplashActivity extends AppCompatActivity {

    //Creating an ImageView
    ImageView imageView;

    //Called when the activity is first created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //This finds the ImageView from the layout
        imageView = (ImageView)findViewById(R.id.coffee_icon);

        //This starts the animation
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.splash_activity);
        imageView.setAnimation(animation);

        //These are sets of instructions the notifies the end, start and repetition of animations.
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            //This particular instruction tells the app to start the main activity ofter the end of animation
            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
